from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import DetailView, ListView, UpdateView

from web.forms import AuthForm, BalanceAddForm, BetForm, CreateUserForm, CommentaryForm, ProfilePhotoForm
from web.models import Bet, Match, Tournament, User, Chat
from web.services import get_matches

# Create your views here.

def hello_world(request):
    get_matches()
    return HttpResponse("Hello, World!")

def now(request):
    #set_result()
    return render(request, 'web/index.html')

class TournamentListView(ListView):
    model = Tournament
    template_name = 'tournament_list.html'
    context_object_name = 'tournaments'

class TournamentDetailView(DetailView):
    model = Tournament
    template_name = 'web/tournament_detail.html'
    context_object_name = 'tournament'

class ListMatchView(ListView, LoginRequiredMixin):
    template_name = 'web/match_list.html'  # Правильное имя шаблона
    model = Match
    context_object_name = 'matches'  # Имя переменной контекста в шаблоне
    paginate_by = 10

class MatchDetailView(DetailView, LoginRequiredMixin):
    model = Match
    template_name = 'web/match_detail.html'
    context_object_name = 'match'


class RegistrationView(View):
    def get(self, request):
        form = CreateUserForm()
        return render(request, "web/registration.html", {"form": form})

    def post(self, request):
        form = CreateUserForm(request.POST, request.FILES)
        if form.is_valid():
            user = form.save()
            messages.success(request, "Вы успешно зарегистрировались")

            # Если пользователь загрузил фото, сохраните его
            if 'image' in request.FILES:
                user.image = request.FILES['image']
                user.save()

            return redirect("auth")
        context = {"form": form}
        return render(request, "web/registration.html", context)
def auth_page(request):
    form = AuthForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            email = form.cleaned_data["email"]
            password = form.cleaned_data["password"]
            user = authenticate(request, email=email, password=password)
            if user is not None:
                login(request, user)
                return redirect("matches/")
            else:
                messages.error(request, "Неверный Логин или Пароль !")
        else:
            messages.error(request, "Некорректные данные !")
    return render(request, "web/auth.html", context={"form": form})



@login_required
def create_bet(request, match_id):
    match = Match.objects.get(pk=match_id)

    if request.method == 'POST':
        form = BetForm(request.POST)
        if form.is_valid():
            bet = form.save(commit=False)

            # Проверяем, есть ли у пользователя достаточно средств для ставки
            if request.user.balance >= bet.amount:
                # Уменьшаем баланс пользователя на сумму ставки
                request.user.balance -= bet.amount
                request.user.save()

                bet.match = match
                bet.user = request.user
                bet.save()
                return redirect('match_detail', pk=match_id)
            else:
                # Если у пользователя недостаточно средств, вы можете вернуть ошибку
                form.add_error(None, 'Недостаточно средств для совершения ставки.')

    else:
        form = BetForm()

    return render(request, 'web/create_bet.html', {'form': form, 'match': match})
class BetUpdateView(UpdateView):
    model = Bet
    form_class = BetForm
    template_name = 'web/bet_update.html'  # Создайте соответствующий шаблон
    success_url = reverse_lazy('user_bets')  # Укажите URL-маршрут для перенаправления после успешного обновления

@login_required
def user_bets(request):
    user_bets = Bet.objects.filter(user=request.user, is_cancelled = False)
    return render(request, 'web/user_bets.html', {'user_bets': user_bets})

def add_balance(request):
    if request.method == 'POST':
        form = BalanceAddForm(request.POST)
        if form.is_valid():
            # Получаем сумму для пополнения из формы
            amount = form.cleaned_data['amount']

            # Пополнение баланса пользователя (предполагается, что у пользователя есть атрибут balance)
            request.user.balance += amount
            request.user.save()

            return redirect('/profile/')  # Маршрут на страницу успешного пополнения
    else:
        form = BalanceAddForm()

    return render(request, 'web/add_balance.html', {'form': form})

def cancel_bet(request, bet_id):
    bet = get_object_or_404(Bet, id=bet_id)

    # Проверяем, что матч еще не считается законченным
    if bet.match.is_active:
        # Если матч не активен, отменяем ставку и возвращаем деньги пользователю
        if not bet.is_cancelled:
            bet.is_cancelled = True
            bet.save()
            print('hello')

            # Возвращаем деньги пользователю
            user = bet.user
            user.balance += bet.amount
            user.save()

    return redirect('/profile')
def logout_view(request):
    logout(request)
    return redirect('/auth')
@login_required
def profile(request):
    return render(request, 'web/profile.html')



def create_chat(request, user_id):
    # Получаем текущего пользователя
    current_user = request.user
    
    try:
        # Проверяем, существует ли чат между текущим пользователем и пользователем user_id
        chat = Chat.objects.filter(participants=current_user).get(participants=user_id)
    except Chat.DoesNotExist:
        # Если чата нет, создаем новый чат
        new_chat = Chat.objects.create()
        new_chat.participants.add(current_user, User.objects.get(pk=user_id))
        chat = new_chat
    
    # Перенаправляем пользователя на страницу чата
    return redirect('chat_view', chat_id=chat.id)


def user_list(request):
    users = User.objects.all()

    context = {
        'users': users,
    }

    return render(request, 'web/user_list.html', context)

def user_detail(request, user_id):
    # Получить объект пользователя или выдать 404 ошибку, если пользователь не существует
    user = get_object_or_404(User, pk=user_id)

    context = {
        'user': user,
    }

    return render(request, 'web/user_detail.html', context)


@login_required
def start_or_open_chat(request, user_id):
    current_user = request.user
    target_user = get_object_or_404(User, pk=user_id)

    # Проверяем, существует ли чат между текущим пользователем и пользователем user_id
    try:
        chat = Chat.objects.filter(participants=current_user).get(participants=target_user)
    except Chat.DoesNotExist:
        # Если чата нет, создаем новый чат
        new_chat = Chat.objects.create()
        new_chat.participants.add(current_user, target_user)
        chat = new_chat

    return redirect('chat_view', chat_id=chat.id)


@login_required
def chat_view(request, chat_id):
    context = {
        'chat_id': chat_id,
    }
    return render(request, 'web/chat.html', context)

@login_required
def add_comment(request, match_id):
    match = Match.objects.get(pk=match_id)

    if request.method == 'POST':
        form = CommentaryForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.match = match
            comment.user = request.user
            comment.save()
            return redirect('match_detail', pk=match_id)
    else:
        form = CommentaryForm()

    return render(request, 'web/add_comment.html', {'form': form, 'match': match})
