import json
from pprint import pprint

import requests
from django.db import transaction
from django.shortcuts import get_object_or_404

from web.api_serializers import TournamentSerializer
from web.models import Sport, Tournament, Team, Match, Bet


def get_matches():
    url = f" https://testkey.odds24.online/v1/tournaments/1/225/live/ru"
    headers = {
            "Package": "studkey14082023"
        }
    footbal = Sport.objects.filter(keys='225')
    if footbal.exists():
        print('hello')
    else:
        new_object = Sport.objects.create(keys='225', name='Футбол', description = 'футбол')
        new_object.save()
    footbal_get = Sport.objects.get(keys='225')
    data = requests.get(url, headers=headers).json()['body']
    #pprint(data.json())

    for tournament_data in data:
        tournament_id = tournament_data['id']

        # Проверяем, существует ли турнир с таким ID в БД
        existing_tournament = Tournament.objects.filter(keyt=tournament_id).first()

        if not existing_tournament:
            tournament = TournamentSerializer(data=tournament_data)
            # print(tournament_data)
            if tournament.is_valid():
                tournament.save()
            tournament = Tournament.objects.get(keyt=tournament_id)
        else:
            tournament = Tournament.objects.get(keyt=tournament_id)

        url_T = f"https://testkey.odds24.online/v1/events/1/{tournament_id}/sub/5/live/ru"
        matches = requests.get(url_T, headers=headers).json()['body'][0]
        print(matches)
        matches = matches.get('events_list', [])

        if len(matches) >0 :
            # Team(name = )

            for game in matches:

                if len(game['game_oc_list']) > 0 and game['game_oc_list'][0]['oc_group_name'] == '1x2'  :
                    game_id = game['game_id']

                    oc1_rate = game['game_oc_list'][0]['oc_rate']
                    oc2_rate = game['game_oc_list'][1]['oc_rate']
                    oc3_rate = game['game_oc_list'][2]['oc_rate']
                    first_team = game['game_oc_list'][0]['oc_name']
                    second_team = game['game_oc_list'][2]['oc_name']

                    existing_team_1 = Team.objects.filter(name=game['game_oc_list'][0]['oc_name']).first()
                    existing_team_2 = Team.objects.filter(name=game['game_oc_list'][2]['oc_name']).first()

                    if not (existing_team_1 and existing_team_2) :

                        team_1 = Team.objects.create(name=first_team, sport=footbal_get, description='описание')
                        team_2 = Team.objects.create(name=second_team, sport=footbal_get, description='описание')

                        team_1.save()
                        team_2.save()

                    else:
                        team_1 = Team.objects.get(name=first_team)
                        team_2 = Team.objects.get(name= second_team)


                    match = Match.objects.filter(keym=game_id).first()

                    if not match:
                        Match.objects.create(keym=game_id, oc1_rate=oc1_rate, oc2_rate=oc2_rate, oc3_rate=oc3_rate,
                                             tournament=tournament, first_team=team_1, second_team=team_2,
                                             sport=footbal_get)



            #print(i['game_id'])

        #tournament = get_object_or_404(Tournament, keyt=tournament_id)
outcome_coefficients = {
    1: 'oc1_rate',
    2: 'oc2_rate',
    3: 'oc3_rate'
}

def set_result():
    inactive_matches = Match.objects.filter(is_active=False, is_payout_completed=False)
    for match in inactive_matches:
        # Получаем все ставки для данного матча
        bets_for_match = Bet.objects.filter(match=match)

        # Проходим по каждой ставке
        for bet in bets_for_match:
            # Проверяем, совпадает ли chosen_outcome с result
            if bet.chosen_outcome == match.result:
                # Получаем соответствующий коэффициент из словаря
                coefficient_field = outcome_coefficients.get(bet.chosen_outcome)

                if coefficient_field:
                    # Получаем значение коэффициента из поля матча
                    coefficient = getattr(match, coefficient_field)

                    # Пополняем баланс пользователя с учетом коэффициента
                    with transaction.atomic():
                        user = bet.user
                        user.balance += coefficient * bet.amount
                        user.save()
            else:
                # Если ставка не успешна, устанавливаем is_winning в False
                bet.is_winning = False
                bet.save()
        match.is_payout_completed = True
        match.save()


