from rest_framework import serializers

from web.models import Tournament


class TournamentSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='keyt')

    class Meta:
        model = Tournament
        fields = ('id', 'name',)
