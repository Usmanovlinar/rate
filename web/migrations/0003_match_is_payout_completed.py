# Generated by Django 4.2.4 on 2023-09-03 13:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0002_bet_is_winning'),
    ]

    operations = [
        migrations.AddField(
            model_name='match',
            name='is_payout_completed',
            field=models.BooleanField(default=False),
        ),
    ]
