from celery import shared_task
from django.core.mail import send_mail
from datetime import timedelta
from django.utils import timezone
from .models import User
from .services import get_matches


@shared_task
def send_inactive_user_reminder():
    get_matches()