from django.utils import timezone

from django.db import models
from django.contrib.auth.models import UserManager as DjangoUserManager
from web.enums import Role
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin

class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class UserManager(DjangoUserManager):
    def _create_user(self, email, password, commit=True, **extra_fields):
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        if commit:
            user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password=None, **extra_fields):
        return self._create_user(email, password, role=Role.admin, **extra_fields)


class User(BaseModel, AbstractBaseUser, PermissionsMixin):
    objects = UserManager()
    image = models.ImageField(upload_to='images', null=True, blank=True)
    username = models.CharField( max_length=30, blank=True)

    email = models.EmailField(unique=True)
    role = models.CharField(choices=Role.choices, max_length=15, default=Role.user)
    name = models.CharField(max_length=255, null=True, blank=True)
    balance = models.DecimalField(max_digits=10, decimal_places=2, default=0)


    @property
    def is_staff(self):
        return self.role in (Role.admin, Role.staff)

    @property
    def is_superuser(self):
        return self.role == Role.admin

    EMAIL_FIELD = "email"
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = "пользователь"
        verbose_name_plural = "пользователи"

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    profile_photo = models.ImageField(upload_to='user_profiles/', blank=True, null=True)



class Sport(models.Model):
    keys = models.CharField(max_length=20, blank=True)
    name = models.CharField(max_length=100)
    description = models.TextField()

class Tournament(models.Model):
    creation_date = models.DateTimeField(default=timezone.now)  # Поле для хранения даты создания
    keyt = models.CharField(max_length=20, blank=True)
    name = models.TextField()

class Team(models.Model):
    name = models.CharField(max_length=100)
    sport = models.ForeignKey(Sport, on_delete=models.CASCADE, )
    description = models.TextField(default=None, blank=True)

class Match(models.Model):
    is_active = models.BooleanField(default=True)
    result = models.PositiveIntegerField(null=True)
    keym = models.CharField(max_length=20, blank=True)
    score_1 = models.IntegerField(default=0, blank=True, )
    score_2 = models.IntegerField(default=0, blank= True)
    oc1_rate = models.DecimalField(max_digits=6, decimal_places=3)
    oc2_rate = models.DecimalField(max_digits=6, decimal_places=3)
    oc3_rate = models.DecimalField(max_digits=6, decimal_places=3)
    tournament = models.ForeignKey(Tournament, on_delete = models.CASCADE)
    first_team = models.ForeignKey(Team, related_name='first_team', on_delete=models.CASCADE)
    second_team = models.ForeignKey(Team, related_name='second_team', on_delete=models.CASCADE)
    sport = models.ForeignKey(Sport, on_delete=models.CASCADE)
    is_payout_completed = models.BooleanField(default=False)


class Commentary(models.Model):
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    comment = models.TextField()

class Bet(models.Model):
    is_winning = models.BooleanField(null=True)
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    chosen_outcome = models.PositiveIntegerField(null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    is_cancelled = models.BooleanField(default=False)



"""class Outcome(models.Model):
    match = models.OneToOneField(Match, on_delete=models.CASCADE)
    result = models.PositiveIntegerField(null=True)"""

"""
class Payments(models.Model):
    bet = models.ForeignKey(Bet, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, decimal_places=2)"""

class Transaction(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    transaction_type = models.CharField(max_length=20)
    
class Chat(models.Model):
    participants = models.ManyToManyField(User, related_name='chats')

class Message(models.Model):
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, related_name='messages')
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sent_messages')
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
# Create your models here.


