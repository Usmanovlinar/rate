"""
URL configuration for rate project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from web import views

urlpatterns = [
    #path('admin/', admin.site.urls),
    path('hello', views.hello_world),
    path('now', views.now),

    path('registration',views.RegistrationView.as_view()),
    path("auth", views.auth_page, name="auth"),
    path('logout/', views.logout_view, name='logout'),

    path('profile/', views.profile, name='profile'),

    path('tournaments/', views.TournamentListView.as_view(), name='tournament_list'),
    path('tournaments/<int:pk>/', views.TournamentDetailView.as_view(), name='tournament_detail'),

    path('matches/', views.ListMatchView.as_view(), name = 'match_list'),
    path('match/<int:match_id>/add_comment/', views.add_comment, name='add_comment'),

    path('match_detail/<int:pk>/', views.MatchDetailView.as_view(), name='match_detail'),
    path('matches/<int:match_id>/bet/', views.create_bet, name='create_bet'),

    path('bets/<int:pk>/update/', views.BetUpdateView.as_view(), name='bet_update'),
    path('cancel_bet/<int:bet_id>/', views.cancel_bet, name='cancel_bet'),

    path('user_bets/', views.user_bets, name='user_bets'),
    path('add_balance/', views.add_balance, name='add_balance'),
    path('create_chat/<int:user_id>/', views.create_chat, name='create_chat'),
    path('user_list/', views.user_list, name='user_list'),
    path('user_detail/<int:user_id>/', views.user_detail, name='user_detail'),
    
    path('start_or_open_chat/<int:user_id>/', views.start_or_open_chat, name='start_or_open_chat'),

    path('chat/<int:chat_id>/', views.chat_view, name='chat_view'),


    path('accounts/', include('allauth.urls')),







    #path('matches/<int:pk>/', views.MatchDetailView.as_view(), name='match_detail'),
]

