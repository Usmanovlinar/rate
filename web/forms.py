
from django import forms
from django.contrib.auth.forms import UserCreationForm

from web.models import User, Bet, Commentary, UserProfile


class BetForm(forms.ModelForm):
    CHOICES = [
        (1, 'Победа первой команды'),
        (2, 'Победа второй команды'),
        (3, 'Ничья'),
    ]

    chosen_outcome = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect)

    class Meta:
        model = Bet
        fields = ['amount', 'chosen_outcome']


class AuthForm(forms.Form):
    email = forms.EmailField(max_length=70)
    password = forms.CharField(widget=forms.PasswordInput)


class CreateUserForm(UserCreationForm):
    name = forms.CharField(max_length=40)
    image = forms.ImageField()
    surname = forms.CharField(max_length=50)
    email = forms.EmailField(max_length=70)
    password1 = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ["name", "surname", "email", 'image']

class BalanceAddForm(forms.Form):
    amount = forms.DecimalField(
        max_digits=10,
        decimal_places=2,
        min_value=0.01,  # Минимальное значение для пополнения
    )


class CommentaryForm(forms.ModelForm):
    class Meta:
        model = Commentary
        fields = ['comment']

class ProfilePhotoForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['profile_photo']

